resource "local_file" "inventory" {
  filename = "hosts"
  file_permission = "0644"
  
  content = templatefile("hosts.tpl", {
    ip_addrsweb = yandex_compute_instance.web[*].network_interface.0.nat_ip_address
    ip_addrnfs = yandex_compute_instance.nfs[*].network_interface.0.nat_ip_address
    
  })
}
resource "local_file" "vars" {
  filename = "vars.yml"
  file_permission = "0644"
  
  content = templatefile("vars.tpl", {

    ip_addrnfs = [ 
      for item in yandex_compute_instance.nfs[*]:
      "${item.name}: ${item.network_interface.0.ip_address}"
    ]
    ip_addrsweb = [ 
      for item in yandex_compute_instance.web[*]:
      "${item.name}: ${item.network_interface.0.ip_address}"
    ]    
  })
}


output "internal_ip_address_web" {
  value = yandex_compute_instance.web[*].network_interface.0.nat_ip_address
}

resource "yandex_vpc_network" "my_network" {
  name = var.my_network
}

resource "yandex_vpc_subnet" "subnet-1" {
  folder_id      = var.folder_id
  name           = var.subnet_name
  zone           = var.yc_zone
  network_id     = yandex_vpc_network.my_network.id
  v4_cidr_blocks = var.subnet_v4_cidr_bloks
}

resource "yandex_vpc_security_group" "alb-sg" {
  name        = "alb-sg"
  network_id  = yandex_vpc_network.my_network.id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }


}



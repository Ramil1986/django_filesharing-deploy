variable "cloud_id" {
  default = "b1glra0lkuq1ul8cjp3h"
} 
variable "folder_id" {
  default = "b1gjcljsutrtc3lq9hl5"
}

variable "token" {}

variable "subnet_v4_cidr_bloks" {
  type    = list(string)
  default = ["192.168.0.0/24"]
}
variable "my_network" {
  type    = string
  default = "my_network"
}
variable "subnet_name" {
  type    = string
  default = "subnet-1"
}

variable "yc_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "default_ssh_key_user" {
  type    = string
  default = "ubuntu"
}

variable "default_ssh_key_path" {
  type    = string
  default = "~/.ssh/terraform_key.pub"
}
variable "default_hdd_type" {
  type    = string
  default = "network-ssd"
}
variable "default_image" {
  type    = string
  default = "ubuntu-2204-lts"
}


variable "web_hostname" {
  type    = string
  default = "web"
}

variable "web_count" {
  type    = number
  default = 1
}

variable "web_platform_id" {
  type    = string
  default = "standard-v2"
}

variable "web_cores" {
  type    = number
  default = 2
}

variable "web_memory" {
  type    = number
  default = 2
}

variable "web_core_fraction" {
  type    = number
  default = 20
}

variable "web_disk_size" {
  type    = number
  default = 8
}



variable "nfs_hostname" {
  type    = string
  default = "files"
}

variable "nfs_count" {
  type    = number
  default = 1
}

variable "nfs_platform_id" {
  type    = string
  default = "standard-v2"
}

variable "nfs_cores" {
  type    = number
  default = 2
}

variable "nfs_memory" {
  type    = number
  default = 2
}

variable "nfs_core_fraction" {
  type    = number
  default = 20
}

variable "nfs_disk_size" {
  type    = number
  default = 8
}



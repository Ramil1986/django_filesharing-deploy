
resource "yandex_compute_image" "ubuntu" {
  source_family = var.default_image
}



resource "yandex_compute_instance" "web" {
  count       = var.web_count
  hostname    = "${var.web_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.web_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.web_platform_id

  resources {
    cores         = var.web_cores
    memory        = var.web_memory
    core_fraction = var.web_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.web_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }
  
 
}



resource "yandex_compute_instance" "nfs" {
  count       = var.nfs_count
  hostname    = "${var.nfs_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.nfs_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.nfs_platform_id

  resources {
    cores         = var.nfs_cores
    memory        = var.nfs_memory
    core_fraction = var.nfs_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.nfs_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }

}

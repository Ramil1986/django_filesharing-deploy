# Инфраструктура

На Хост машине произвести установку приложений:
- gitlab-runner

Затем подключить gitlab-runner к проекту.

Также для работы проекта необходимо настроить Variables gitlab:
- AWS_ACCESS_KEY_ID (access key сервисного аккаунта, который будт работать с s3 backend)
- AWS_SECRET_ACCESS_KEY (secret key сервисного аккаунта, который будт работать с s3 backend)
- YC_KEY (key сервисного аккаунта, который будtт работать с yandex cloud)
- yandex_token (токен для работы с yandexcloud)


## Vars

Для корректной работы приложения предусмотрены следующие VARSы:   
| Название                    | Значение по умолчанию | Описание                                    |
|-----------------------------|-----------------------|---------------------------------------------|
| `user_name`             | `ubuntu`         | имя пользователя линукс |
| `user_group`            | `ubuntu`       | имя группы пользователя линукс             |
| `address`       | `192.168.0.0/24` | подсеть NFS                 |  
| `uid`               | `1000`        | UID юзера NFS сервера                  |
| `gid`              | `1000`                | GID юзера NFS сервера                                |

## Roles
nfs - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/nfs

docker - https://https://gitlab.com/ramil_nizamov1986/ansible/my-roles/docker

nginx - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/nginx

compose-deploy - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/compose_deploy


